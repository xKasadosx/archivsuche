import os
import pymssql
import time
from pathlib import Path
directorylist=[]

def connect_to_database():
    global db_connect
    db_connect = pymssql.connect(
        server="",
        user="",
        password="",
        database="",
    )

def get_all_pdfs_from_db(prefix):
    sqlQuery = "select pdf_name from filearchiv where file_prefix = '" + str(prefix) + "'"
    query = db_connect.cursor()
    query.execute(sqlQuery)
    result = query.fetchall()
    if(result != None):
        return result
    else:
        return None



def insert_new_customer(pdfPath,pdfName,filePrefix):
    try:
        sqlInsert = """INSERT INTO filearchiv (pdf_name, file_path, file_prefix) VALUES('%s','%s','%s')""" %(pdfName,pdfPath,filePrefix)

        insert = db_connect.cursor()
        insert.execute(sqlInsert)
        
        db_connect.commit()
        db_connect.close
        return 1
    except (pymssql.Error, pymssql.Warning) as e:
        print(e)
        return 0


## überarbeiten mit dict.
def get_file_prefix(directorys):
    filePrefix = ""
    if("Rollkarten" in directorys):
        filePrefix = "RK"
    elif("Entladelisten" in directorys):
        filePrefix = "EL"
    elif("Eingangsfrachtkarten" in directorys):
        filePrefix = "EF"
    elif("Kundenlieferscheine" in directorys):
        filePrefix = "KL"
    elif("Kommissionierungsschein" in directorys):
        filePrefix = "KS"
    elif("Ablieferbelege" in directorys):
        filePrefix = "AB"
    elif("Lagerkontrolle" in directorys):
        filePrefix = "LK"
    elif("AbrechnungSE" in directorys):
        filePrefix = "ASE"
    elif("Beschaffung" in directorys):
        filePrefix = "BS"
    else:
        filePrefix = "UNKNOWN"
    return filePrefix

def get_new_pdfs(directorys):
    filePrefix = get_file_prefix(directorys)
    print("Daten: " + str(filePrefix))
    databaseData = get_all_pdfs_from_db(filePrefix)
    print("Datenbankeinträge geholt\n -> "+str(len(databaseData)) + " Dateien")
    databaseList =[]
    for data in databaseData:
        databaseList.append(data[0])
    fileList = os.listdir(directorys)
    print("Ordner aufgelistet\n -> " +str(len(fileList)) + " Dateien")
    print("Listen werden verglichen")
    databaseSet = set(databaseList)
    liste =[]
    liste = [x for x in fileList if x not in databaseSet]
    endlist=[]
    print("1 fertig")

    ##test###
    testliste = []
    fileset = set(fileList)
    for x in databaseSet:
        if x not in fileset:
            print(x)
            testliste.append(x)
    for x in testliste:
        delete_pdf(x, filePrefix)
    ##test###



    print("Überprüfung auf PDF Datei")
    for item in liste:
        if(item.endswith(".pdf")):
            endlist.append(item)
    print("Differenz: " + str(len(endlist)) + " Datein")
    if(len(endlist) >= 1):
        print("Daten werden an die DB übermittelt...")
        for pdfName in endlist:
            insert_new_customer(directorys,pdfName,filePrefix)
        print("Daten übermittelt.")
    else:
        print("Keine Daten zu übermitteln.")
    print("_____________________________________")


def get_all_directorys(directorylist):
    for directorys in directorylist:
        get_new_pdfs(directorys)


def delete_pdf(pdfName, filePrefix):
    try:
        sql_delete = """DELETE FROM filearchiv where pdf_name = '%s' and file_prefix = '%s' """ % (pdfName, filePrefix)
        insert = db_connect.cursor()
        insert.execute(sql_delete)
        db_connect.commit()
        db_connect.close
        return 1
    except (pymssql.Error, pymssql.Warning) as e:
        print(e)
    return 0

connect_to_database()
get_all_directorys(directorylist)
db_connect.close()
time.sleep(5)

