import sys
import os
import time
from PyQt5 import QtGui
from qtpy import QtWidgets
# from qtpy import uic
from ui.mainwindow import Ui_MainWindow
import pymssql
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject, QThread, pyqtSignal

os.environ['QT_API'] = 'pyqt'

# uic.compileUiDir("ui")

# connection to database


def connect_to_database():
    db_connection = pymssql.connect(
        server="",
        user="",
        password="",
        database=""
    )
    return db_connection


# worker class
class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    # führt den gewollten task aus
    def get_database_data(self):
        get_data_from_database()
        # self.progress.emit(TEXT ODER SO )
        self.finished.emit()

    def open_pdf(self):
        select_data_tuple()
        self.finished.emit()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowIcon(QtGui.QIcon('./Styles/Combinear/suche.ico'))
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButtonSearch.clicked.connect(self.get_data)
        self.ui.pushButtonAll.clicked.connect(self.check_all_boxes)
        self.ui.pushButtonNone.clicked.connect(self.uncheck_all_boxes)
        self.label_result = self.ui.labelResult
        # initalize all checkboxen on true
        self.checkbox_rk = self.ui.checkBoxRK.setChecked(1)
        self.checkbox_el = self.ui.checkBoxEL.setChecked(1)
        self.checkbox_ef = self.ui.checkBoxEF.setChecked(1)
        self.checkbox_kl = self.ui.checkBoxKL.setChecked(1)
        self.checkbox_ks = self.ui.checkBoxKS.setChecked(1)
        self.checkbox_ab = self.ui.checkBoxAB.setChecked(1)
        self.checkbox_lk = self.ui.checkBoxLK.setChecked(1)
        self.checkbox_ase = self.ui.checkBoxASE.setChecked(1)
        self.checkbox_bs = self.ui.checkBoxBS.setChecked(1)
        self.header = self.ui.tableWidget.horizontalHeader()
        self.ui.tableWidget.setColumnWidth(0, 60)
        self.header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        self.ui.tableWidget.doubleClicked.connect(self.open_pdf)
        self.ui.lineEditUserInput.returnPressed.connect(self.ui.pushButtonSearch.click)

    def get_data(self):
        self.thread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.get_database_data)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        # updater des progresses
        # self.worker.progress.connect(self.reportProgress)
        # startet den progress
        self.thread.start()
        # deaktiviert und aktiviert den button
        # aktiviert button wenn fertig.
        self.ui.pushButtonSearch.setEnabled(False)
        self.thread.finished.connect(
            lambda: self.ui.pushButtonSearch.setEnabled(True)
        )

    def open_pdf(self):
        self.thread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.open_pdf)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        # updater des progresses
        # self.worker.progress.connect(self.reportProgress)
        # startet den progress
        self.thread.start()
        # deaktiviert und aktiviert den button
        # aktiviert button wenn fertig.
        self.ui.pushButtonSearch.setEnabled(False)
        self.ui.tableWidget.setEnabled(False)
        self.thread.finished.connect(
            lambda: self.ui.pushButtonSearch.setEnabled(True)
        )
        self.thread.finished.connect(
            lambda: self.ui.tableWidget.setEnabled(True)
        )

    def uncheck_all_boxes(self):
        self.ui.checkBoxRK.setChecked(0)
        self.ui.checkBoxEL.setChecked(0)
        self.ui.checkBoxEF.setChecked(0)
        self.ui.checkBoxKL.setChecked(0)
        self.ui.checkBoxKS.setChecked(0)
        self.ui.checkBoxAB.setChecked(0)
        self.ui.checkBoxLK.setChecked(0)
        self.ui.checkBoxASE.setChecked(0)
        self.ui.checkBoxBS.setChecked(0)

    def check_all_boxes(self):
        self.ui.checkBoxRK.setChecked(1)
        self.ui.checkBoxEL.setChecked(1)
        self.ui.checkBoxEF.setChecked(1)
        self.ui.checkBoxKL.setChecked(1)
        self.ui.checkBoxKS.setChecked(1)
        self.ui.checkBoxAB.setChecked(1)
        self.ui.checkBoxLK.setChecked(1)
        self.ui.checkBoxASE.setChecked(1)
        self.ui.checkBoxBS.setChecked(1)

    def get_checked_checkboxen_for_db(self):
        checkbox_file_categories = []
        if self.ui.checkBoxRK.isChecked():
            checkbox_file_categories.append("RK")
        if self.ui.checkBoxEL.isChecked():
            checkbox_file_categories.append("EL")
        if self.ui.checkBoxEF.isChecked():
            checkbox_file_categories.append("EF")
        if self.ui.checkBoxKL.isChecked():
            checkbox_file_categories.append("KL")
        if self.ui.checkBoxKS.isChecked():
            checkbox_file_categories.append("KS")
        if self.ui.checkBoxAB.isChecked():
            checkbox_file_categories.append("AB")
        if self.ui.checkBoxLK.isChecked():
            checkbox_file_categories.append("LK")
        if self.ui.checkBoxASE.isChecked():
            checkbox_file_categories.append("ASE")
        if self.ui.checkBoxBS.isChecked():
            checkbox_file_categories.append("BS")
        return checkbox_file_categories

    def insert_into_view(self, result):
        self.ui.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.ui.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                item = QTableWidgetItem(str(data))
                item.setTextAlignment(Qt.AlignCenter)
                self.ui.tableWidget.setItem(row_number, column_number, item)


def delete_corrupt_data(pdf_name):
    db_connection = connect_to_database()
    try:
        db_delete_query = "delete from filearchiv where pdf_name = '" + str(pdf_name) + "'"
        db_cursor = db_connection.cursor()
        db_cursor.execute(db_delete_query)
        db_connection.commit()
    except (pymssql.Error, pymssql.Warning):
        print("")
    finally:
        db_connection.close()


def select_data_tuple():
    db_file_data = find_path_from_database(window.ui.tableWidget.item(window.ui.tableWidget.currentRow(), 1).text())
    if db_file_data is not None:
        file_path = db_file_data[0] + "/" + db_file_data[1]
        file_path = file_path.replace("/", "\\")
        error = 0
        try:
            window.label_result.setStyleSheet('color: white')
            window.label_result.setText("Datei wird geöffnet...")
            os.startfile(file_path)
        except OSError:
            error = 1
        finally:
            if error == 1:
                window.label_result.setStyleSheet('color: red')
                window.label_result.setText("Kein Zugriff auf Archiv vorhanden.\nBitte beim Administrator melden!")
            else:
                window.label_result.setText("Datei wurde geöffnet.")

    else:
        window.label_result.setStyleSheet('color: red')
        window.label_result.setText("Datenbank ist Offline!\nBeim Administrator melden!")


def find_pdf_from_database(user_search_request, checkbox_file_categories):
    # if 9 are checked
    if len(checkbox_file_categories) == 9:
        sql_select_query = "select TOP 5000 file_prefix, pdf_name from filearchiv where pdf_name like '%" + str(
            user_search_request) + "%'"
    # if 2-8 are checked
    elif 1 < len(checkbox_file_categories) < 9:
        sql_query_ending = " and ( file_prefix = '"
        category_counter = 0
        for category in checkbox_file_categories:
            category_counter = category_counter + 1
            if category_counter == 1:
                sql_query_ending = sql_query_ending + str(category) + "'"
            if category_counter > 1:
                sql_query_ending = sql_query_ending + " or file_prefix = '" + category + "'"
        sql_select_query = "select TOP 5000 file_prefix, pdf_name from filearchiv where pdf_name like '%" + user_search_request + "%'" + sql_query_ending + " ) order by pdf_name asc"
    else:
        sql_query_ending = " and file_prefix = '" + checkbox_file_categories[0] + "'"
        sql_select_query = "select TOP 5000 file_prefix, pdf_name from filearchiv where pdf_name like '%" + str(
            user_search_request) + "%'" + str(sql_query_ending) + " order by pdf_name asc"
    try:
        db_connect = connect_to_database()
    except pymssql.Error:
        return None
    try:
        query = db_connect.cursor()
        query.execute(sql_select_query)
        result = query.fetchall()
        if result is not None:
            return result
        else:
            return None
    except (pymssql.Error, pymssql.Warning):
        return None
    finally:
        db_connect.close()


def find_path_from_database(pdf_name):
    try:
        db_connect = connect_to_database()
    except (pymssql.Error, pymssql.Warning):
        window.label_result.setStyleSheet('color: red')
        window.label_result.setText(
            "Kein Datenbankzugriff / Datenbank offline!")
        return None
    try:
        sql_query = "select file_path,pdf_name from filearchiv where pdf_name = '" + str(pdf_name) + "'"
        query = db_connect.cursor()
        query.execute(sql_query)
        result = query.fetchone()
        if result is not None:
            return result
        else:
            return None
    except (pymssql.Error, pymssql.Warning):
        return None
    finally:
        db_connect.close()


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()


def get_data_from_database():
    window.ui.tableWidget.setRowCount(0)
    search_time = time.time()
    checkbox_file_categories = window.get_checked_checkboxen_for_db()
    user_search_request = window.ui.lineEditUserInput.text()
    if len(checkbox_file_categories) > 0:
        result = find_pdf_from_database(user_search_request, checkbox_file_categories)
        if result is not None:
            count_data = len(result)
            if len(result) > 0:
                window.insert_into_view(result)
                search_time = round(time.time() - search_time, 2)
                window.label_result.setStyleSheet('color: white')
                window.label_result.setText(
                    "Dateien: " + str(count_data) + "\nSuchzeit: " + str(search_time) + " Sekunde(n)")
            else:
                search_time = round(time.time() - search_time, 2)
                window.label_result.setStyleSheet('color: red')
                window.label_result.setText(
                    "Dateien: Keine Daten gefunden.\nSuchzeit: " + str(search_time) + " Sekunde(n)")
        else:
            window.label_result.setStyleSheet('color: red')
            window.label_result.setText(
                "Die Datenbank ist offline.\nBitte beim Administrator melden!")
    else:
        window.label_result.setStyleSheet('color: red')
        window.label_result.setText(
            "Mindestens eine Kategorie auswählen!")


Design = open("./Styles/Combinear/Combinear.qss", 'r')

with Design:
    qss = Design.read()
    app.setStyleSheet(qss)

window.show()

sys.exit(app.exec_())
